## Sum of Numbers description

You should create an CLI application that takes a sum as a string in either digits, spanish or english
and returns its result as string in the same language (digits/spanish/english).

The string will be two numbers and an infix operator (+/mas/plus).

Max number is 999 and min number is 0.

The entry point of the CLI is the file `calculate`. This file already already exists in the repository but implementation is missing.

## Implementation

The challenge has been programmed using Typescript.
It implements an useful user interface for input and output, and the calculator functionality is wrapped inside it.

The main entrypoint calculate expects input inserted by the user interface and then calls the Calculator statis class for compute the numbers and operations.

It is implemented an abstraction about the language using inherited class for prevent duplicated coded, and specific implementation for every language.
I implemented the program as this way thinking about to make it easier if we want to add extra languages.

The specific rules of each language are isolated in the correspondent class, and the generic numeric handling and sintaxes are being separated as well.

Each class has a validate method where every checks are isolated, making the program safe to run and not mixing purposes.
This way of work makes the program easy to read as well (The algorithm specification makes the code complex, tho).

Each class included as well a dictionary for the specific language, running as a source of truth,
and it will be the place where program will check it the words are valid, and where the program will have the value for the number words.

![Arquitecture](./images/internalArq.png)

## Tests

The suggested test suite was refactored to accomodate my specific implementation, but the cases are exactly the same, showing that the program works as expected.
I decided not to add more extra tests because running out of time reasons, but my program implementation covers more issues and incorrect cases than the suggested tests.
They can be shown on the errors throwns by the program and valiation methods.

## How to use

### Preparing the startup

The program only needs the internal dependencies defined in package.json file.
To install them, just typeon terminal, being placed in the project's main folder:

```
$ npm install
```

### Use the calculator

To start the program, type on terminal:

```
$ npm start
```

An user interface will appears at the terminal, showing the details of the program and expecting an input by keyboard.
Pressing enter after writing the input will sent the parameter to the calculator, starting the operation, checking the operation itself, the numbers to plus,
and the correct wording for every number. It works with numbers, English number words and Spanish as well.

![Arquitecture](./images/program_scopes.png)

Is possible to close the program directly writing EXIT/exit or closing the program with the SIGINT signal with CTRL + C keys.

The program will show a message by terminal after their process:

- If there is errors on input, the program will show them.
- If there is no operator detected in the input, the program will validate the number and show the same value if there is no errors.
- If there is one operator mark in the input, the program will validate the full operation, and the numbers to plus as well.

After the program's response, the user interface will ask the user if he wants to make another extra operation.

The program will await the user response until the user write YES/yes/y/Y or NO/no/n/N. It the response is negative, the program will close. If not, the interface will prompt the initial message and will expect another user input.

### Testing the calculator

In order to run the tests, it only requires to type on the terminal:

```
$ npm test
```

All the detected test will run and show the result by terminal.
