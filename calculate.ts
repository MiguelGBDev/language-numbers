import { askNextSum, printScreen } from './entities/userInterface/userInterface'
import { Calculator } from './entities/calculator/calculator'

let input = ''
while (true) {
  input = printScreen()
  if (input.toLowerCase() === 'exit') process.exit(1)

  if (input) {
    try {
      const result = Calculator.start(input)
      console.info(`${input} = ${result}\n`)
    } catch (error) {
      console.error(`ERROR: ${error?.message}\n`)
    }
  }

  askNextSum()
  console.clear()
}
