enum position {
  NOT_FOUND = 'NOT_FOUND',
  UNITS = 'UNITS',
  TENS = 'TENS',
  CENTS = 'CENTS',
  THOUSAND = 'THOUSAND',
  MINUS = 'MINUS'
}

export interface Dictionary {
  CENTS: { [key: string]: number }
  TENS: { [key: string]: number }
  COMPLEXES_TENS: { [key: string]: number }
  UNITS: { [key: string]: number }
}

export interface SpecialChars {
  AND: string
  MINUS: string
  THOUSAND: string
}

type ValueCounters = {
  [key in keyof typeof position]: number
}

/**
 * Formater (static)
 *
 * This class validates the input.
 *
 * It checks several constraints:
 *
 * - the correct order
 * - correct words
 * - correct syntax
 * - correct thresholds
 *
 * The main method validate is responsible of throws the specic error.
 * If there is no error, the method finishes and let the program to continue.
 *
 * The rest of methods are auxiliary for validate, and they are not visible outside the class.
 */
export abstract class Formatter {
  /**
   * Main method of formatter class.
   *
   * Checks splittedNumbers, an array composed by every word of the input,
   * and checks the order, sintax, thresholds of the input.
   * Also it checks if the words exist in the dictionary passed as parameters.
   *
   * This method is language agnostic.
   * If only checks the main conditions about how numbers are formed.
   *
   * If there is one error in the input, it throws the specific error.
   * Otherwise, the parameter is condidered valid, and let the program continue.
   *
   */
  public static validate(splittedNumbers: string[], composedDictionary: Dictionary, specialChars: SpecialChars) {
    const orderValues = this.sortWordType(splittedNumbers, composedDictionary, specialChars)
    const valueCounters = this.countTypes(orderValues)
    const isCorrectOrder = this.verifyOrder(orderValues)

    if (splittedNumbers.includes(specialChars.MINUS) || valueCounters.THOUSAND > 0)
      throw new Error('some number is out of range (0-999)')

    if (valueCounters.NOT_FOUND > 0) throw new Error(`Some number is incorrect`)

    const atLeastOneNumber = !!valueCounters.CENTS || !!valueCounters.TENS || !!valueCounters.UNITS
    if (!atLeastOneNumber) throw new Error(`No number detected`)

    const nonRepeatedCounters = valueCounters.CENTS < 2 && valueCounters.TENS < 2 && valueCounters.UNITS < 2
    if (!nonRepeatedCounters) throw new Error(`There are several numbers for the same position`)

    if (!isCorrectOrder) throw new Error(`The numbers are messed up`)
  }

  /**
   * sortWordType (private static)
   *
   * This method checks the words inside the parameter
   * and make an array ordered by the words' apparition.
   *
   * If some word is not recongnized, it is considered NOT_FOUND type.
   *
   * The method returns the composed array of those types.
   */
  private static sortWordType(splittedNumbers: string[], composedDictionary, specialChars: SpecialChars): position[] {
    const { CENTS, TENS, COMPLEXES_TENS, UNITS } = composedDictionary
    const { AND, THOUSAND, MINUS } = specialChars
    const order: position[] = []

    for (const elem of splittedNumbers) {
      if (!!CENTS[elem]) {
        order.push(position.CENTS)
      } else if (!!COMPLEXES_TENS[elem]) {
        order.push(position.TENS, position.UNITS)
      } else if (!!TENS[elem]) {
        order.push(position.TENS)
      } else if (UNITS.hasOwnProperty(elem)) {
        order.push(position.UNITS)
      } else if (elem.includes(THOUSAND)) {
        order.push(position.THOUSAND)
      } else if (elem.includes(MINUS)) {
        order.push(position.MINUS)
      } else if (elem !== AND) {
        order.push(position.NOT_FOUND)
      }
    }

    return order
  }

  /**
   * countTypes (private static)
   *
   * This method receive an ordered array with the type of each word inside the parameter.
   * It Counts the apparitions of each type and return the result.
   */
  private static countTypes(orderValues: position[]): ValueCounters {
    const valueCounters: Record<position, number> = {
      [position.NOT_FOUND]: 0,
      [position.UNITS]: 0,
      [position.TENS]: 0,
      [position.CENTS]: 0,
      [position.THOUSAND]: 0,
      [position.MINUS]: 0
    }

    for (const val of orderValues) {
      valueCounters[val]++
    }

    return valueCounters
  }

  /**
   * verifyOrder (private static)
   *
   * This method assigns weights to each word type.
   * Checks if the correct order inside the number word composition.
   *
   * Returns true for valid number composition or false is incorrect.
   */
  private static verifyOrder(orderValues: position[]): boolean {
    let prev = 99999
    const WEIGHTS = { [position.CENTS]: 3, [position.TENS]: 2, [position.UNITS]: 1 }

    for (const elem of orderValues) {
      if (WEIGHTS[elem] > prev) return false

      prev = WEIGHTS[elem]
    }

    return true
  }
}
