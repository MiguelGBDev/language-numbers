/**
 * NumberClass
 *
 * Parent class inherited by every language implementation.
 *
 * It includes the main methods required for the numbers manipulation.
 * These methods will be consumed or overrided by child classes.
 *
 * By default this class works only with numbers, so it is agnostic about any language.
 * It is responsible of numeric inputs as well.
 */
export class NumberClass {
  protected _splittedNumbers: string[]
  protected AND: string
  protected dictionary: { [key: string]: number }
  protected _value: number

  /**
   * Constructor
   *
   * Checks if the parameter could be converted to integer.
   * If not, does nothing.
   */
  constructor(inputNumber: string) {
    if (Number.isInteger(+inputNumber)) {
      this._value = parseInt(inputNumber)
    }
  }

  /**
   * exists (public static)
   *
   * This method checks if the first character of the input is numeric
   * Returning true for that case.
   * Otherwise, returns false.
   */
  public static exists(inputString: string): boolean {
    const { 0: firstWord } = inputString.split(' ')
    return Number.isInteger(+firstWord)
  }

  /**
   * Getter value
   *
   * Returns the value for the class property value
   */
  public get value() {
    return this._value
  }

  /**
   * Getter splittedNumbers
   *
   * Returns the string array with the words of the input
   * (if it exists)
   */
  public get splittedNumbers(): string[] {
    return this._splittedNumbers
  }

  /**
   * validate
   *
   * This validate method simply checks if the class property value number
   * fits the application expected threshold.
   *
   * It not, throws error.
   * Otherwise, returns true.
   */
  public validate() {
    if (this._value > 999 || this._value < 0) throw new Error('some number is out of range (0-999)')
  }

  /**
   * computeValue
   *
   * This methods return the stringied value class property
   */
  public computeValue(): string {
    return `${this._value}`
  }
}
