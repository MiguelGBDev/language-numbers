export const UNITS = {
  zero: 0,
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9
}

export const COMPLEXES_TENS = {
  ten: 10,
  eleven: 11,
  twelve: 12,
  thirteen: 13,
  fourteen: 14,
  fifteen: 15,
  sisteen: 16,
  seventeen: 17,
  eighteen: 18,
  nineteen: 19
}

export const TENS = {
  ...COMPLEXES_TENS,
  twenty: 20,
  thirty: 30,
  forty: 40,
  fifty: 50,
  sixty: 60,
  seventy: 70,
  eighty: 80,
  ninety: 90
}

export const CENTS = {
  one_hundred: 100,
  two_hundred: 200,
  three_hundred: 300,
  four_hundred: 400,
  five_hundred: 500,
  six_hundred: 600,
  seven_hundred: 700,
  eight_hundred: 800,
  nine_hundred: 900
}

export const DICTIONARY = {
  ...UNITS,
  ...TENS,
  ...CENTS
}

export const THOUSAND = 'one_thousand'
export const MINUS = 'minus'
export const AND = '-'
