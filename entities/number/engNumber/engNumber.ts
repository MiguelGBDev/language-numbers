import { NumberClass } from '../numberClass'
import { Formatter, Dictionary } from '../../formatter/formatter'
import { DICTIONARY, CENTS, TENS, COMPLEXES_TENS, UNITS, THOUSAND, MINUS, AND } from './engDictionary'

/**
 * English Number (inherit NumberClass)
 *
 * This class manages English numbers
 * It controls the details of the English language
 * and its correct specification.
 */
export class EnglishNumber extends NumberClass {
  /**
   * Constructor
   *
   * It receives the parameter and splits it separated by spaces for a correct handling.
   *
   * To prevent posterior issues, it transform some words:
   * 'one thousand' will be replaced by one_thousand (_) to prevent detect the one as units'
   * Same for 'hundred', in order to detect it corretly as cents is added a underscore to define it as cent.
   *
   * Then, the full string is splitted by spaces as an array of words and store it as a class attribute.
   */
  constructor(inputString: string) {
    super(inputString)
    this.dictionary = DICTIONARY

    if (typeof inputString === 'string') {
      const stringReplaced = inputString.replace(' hu', '_hu').replace('-', ' - ').replace('ne tho', 'ne_tho')

      super._splittedNumbers = stringReplaced.split(' ').filter(elem => elem)
    }
  }

  /**
   * exists (public static)
   *
   * This method checks the words list to search them in the English dictionary.
   *
   * If some word is considered a English word, it returns true.
   * Otherwise, returns false.
   */
  public static exists(inputString: string): boolean {
    const splittedString = inputString.split(' ')
    for (const word of splittedString) if (DICTIONARY.hasOwnProperty(word)) return true

    return false
  }

  /**
   * validate
   *
   * this method use Formatter static class to check if the full parameter is correct.
   * If there is something incorrect, formatter will throw an specific error.
   *
   * Otherwise, Formatter.validate and the validate method itself
   * will continue with the correct program execution.
   */
  public validate() {
    const composedDictionary: Dictionary = { CENTS, TENS, COMPLEXES_TENS, UNITS }
    const specialChars = { AND, MINUS, THOUSAND }
    Formatter.validate(this.splittedNumbers, composedDictionary, specialChars)
  }

  /**
   * getter value
   *
   * This methods checks the value of every word in the English dictionary.
   * Counts every value of the parameter's words
   * and return the composed value.
   */
  public get value() {
    let resultNumber = 0
    const splittedNumbers = this._splittedNumbers.filter(elem => elem !== AND)
    for (const inputNumber of splittedNumbers) resultNumber += this.dictionary[inputNumber]

    return resultNumber
  }

  /**
   * computeValue
   *
   * This method is used to transform a numeric result to the English language words.
   * It reverses the english dictionary and search the word used in English
   * for the digits that are inside the class property value.
   *
   * It manages the specific sintax for English numeric composition.
   */
  public computeValue(): string {
    const REVERSE_DICTIONARY = EnglishNumber.flipDictionary()
    const ZERO = REVERSE_DICTIONARY['0']

    let stringNumber = super.value.toString()
    let thousands = ''
    if (stringNumber.length === 4) {
      thousands = THOUSAND
      stringNumber = stringNumber.substring(1)
    }

    let reverseStringNumber = ''
    for (let i = stringNumber.length - 1; i >= 0; i--) {
      reverseStringNumber += stringNumber[i]
    }

    const cents = REVERSE_DICTIONARY[`${reverseStringNumber[2]}00`] || ''
    const complex_tens = REVERSE_DICTIONARY[`${reverseStringNumber[1]}${reverseStringNumber[0]}`] || ''
    const tens = REVERSE_DICTIONARY[`${reverseStringNumber[1]}0`] || ''
    let units = REVERSE_DICTIONARY[reverseStringNumber[0]] || ''

    if (units === ZERO) units = !cents && !tens ? ZERO : ''

    let result = ''
    result += ` ${cents}`
    if (complex_tens) result += ` ${complex_tens}`
    else {
      result += ` ${tens}`
      if (!!tens && !!units) result += AND
      result += units
    }

    result = result.trim()
    result = thousands ? `${thousands} ${result}` : result
    return result.replaceAll('_', ' ')
  }

  /**
   * flipDictionary (private static)
   *
   * This auxiliary method takes the English dictionary
   * and reverse it.
   *
   * The dictionary by default includes words as keys,
   * with their correspondent number value as value.
   *
   * The method reverse the dictionary, making another dictionary
   * with the (stringified) numbers as keys, and the words as values.
   */
  private static flipDictionary(): { [key: string]: string } {
    return Object.fromEntries(Object.entries(DICTIONARY).map(([key, value]) => [value, key]))
  }
}
