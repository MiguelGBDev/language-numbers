import promptSync from 'prompt-sync'
const prompt = promptSync({ sigint: true })

/**
 * printScreen
 *
 * This method prints the user interface on the terminal.
 *
 * It expects an input by keyboard.
 * If the input is EXIT or exit, the program will close.
 *
 * Otherwise, the method will return the input.
 */
export const printScreen = (): string => {
  let input = ''

  console.info('-----------CALCULATOR---------')
  console.info('------------------------------')
  console.info('Format: number1 + number2')
  console.info('Numbers can be integers or strings')
  console.info(`Examples: 'trescientos cuarenta y uno', '32', 'forty-two'`)
  console.info(`** TYPE 'EXIT' or 'ctrl + c' TO CLOSE THE PROGRAM ** \n`)

  input = prompt('Please insert the desired sum: ')
  console.info()

  return input
}

/**
 * askNextSum
 *
 * Once the previous method sent the input to the main program,
 * this method will ask if the user wants to make another operation.
 *
 * If the input is yes (yes/YES/y/Y) the program will continue.
 * Otherwise, the program will close.
 *
 */
export const askNextSum = (): void => {
  const YES_VALUES = ['yes', 'y']
  const NO_VALUES = ['no', 'n']
  const allowedResponses = [...YES_VALUES, ...NO_VALUES]

  let input = ''
  while (!allowedResponses.includes(input)) {
    console.info(`Do you want to make another sum?`)
    input = prompt(`yes(y) or no(n): `)
    input = input.toLowerCase()
  }

  if (NO_VALUES.includes(input)) {
    process.exit(1)
  }
}
