import { SpanishNumber } from '../number/spaNumber/spaNumber'
import { EnglishNumber } from '../number/engNumber/engNumber'
import { NumberClass } from '../number/numberClass'

enum LANGUAGE {
  NUMBER = 'number',
  SPANISH = 'spanish',
  ENGLISH = 'english',
  NONE = 'none'
}

enum OPERATOR {
  '+' = '+',
  PLUS = 'plus',
  MAS = 'mas'
}

const OPERATOR_LANG: { language: LANGUAGE; operator: OPERATOR }[] = [
  { language: LANGUAGE.ENGLISH, operator: OPERATOR.PLUS },
  { language: LANGUAGE.SPANISH, operator: OPERATOR.MAS },
  { language: LANGUAGE.NUMBER, operator: OPERATOR['+'] },
  { language: LANGUAGE.NONE, operator: undefined }
]

/**
 * Calculator Class (static)
 *
 * This class is the entrypoint of data once they have been inserted by the user.
 *
 * It checks if the input is correct, and return the result.
 *
 * It gets the input and try to find what is it language.
 * It checks if the input includes the plus operator, If it is existing in the input,
 * it calculates the numeric result, and then convert it to the language of the input.
 *
 * If there is no operator detected, it validate the input and then return the result.
 *
 * If the input is invalid, it throws an error with the reason.
 */

export abstract class Calculator {
  /**
   * start method (static)
   *
   * Checks the language checking if the words exits in languages' dictionaries.
   * Once the language is detected, it gets the correct operator and tries to make the sum.
   *
   * If there is no operator, it validates the input as a single parameter
   * and if it is correct, return the same value.
   *
   * If the operator is inside the input, calculate the numeric result
   * and then convert it to the requested language, returning it outside
   */
  public static start(inputString: string): string {
    if (!inputString) throw new Error('empty input')

    const lang = this.checkLanguage(inputString)
    const operator = OPERATOR_LANG.find(elem => elem.language === lang)?.operator

    let LangClass
    if (lang === LANGUAGE.SPANISH) {
      LangClass = SpanishNumber
    } else if (lang === LANGUAGE.ENGLISH) {
      LangClass = EnglishNumber
    } else if (lang === LANGUAGE.NUMBER) {
      LangClass = NumberClass
    } else throw new Error('invalid string')

    if (!inputString.includes(operator)) {
      const numberInstance = new LangClass(inputString)
      numberInstance.validate()
      return inputString
    }

    const splittedOperation = inputString.split(operator)
    const numberInstance1: NumberClass = new LangClass(splittedOperation[0])
    const numberInstance2: NumberClass = new LangClass(splittedOperation[1])

    this.validateOperation(splittedOperation, numberInstance1, numberInstance2)

    const resultNumber = numberInstance1.value + numberInstance2.value
    return new LangClass(resultNumber).computeValue()
  }

  /**
   * checkLanguage (static)
   *
   * Search the input in the languages dictionary.
   *
   * Return the language if the input exists in the dictionary.
   * If not, checks if the input is numeric.
   *
   * If not, the language is not found, returning NONE value.
   */
  private static checkLanguage(inputString: string): LANGUAGE {
    if (SpanishNumber.exists(inputString)) return LANGUAGE.SPANISH
    else if (EnglishNumber.exists(inputString)) return LANGUAGE.ENGLISH
    else if (NumberClass.exists(inputString)) return LANGUAGE.NUMBER

    return LANGUAGE.NONE
  }

  /**
   * validateOperation
   *
   * Checks if the braces of the operation are correct.
   * If the operation included more than to parameters to sum
   * or if there are more than one operator, it throws an error.
   *
   * After that, the method checks if the two parameters to sum are correct.
   * If the operation or the parameters are incorrect,
   * this method will throw an error with the specific reason.
   */
  private static validateOperation(splittedString: string[], number1: NumberClass, number2: NumberClass): void {
    if (splittedString.length !== 2) throw new Error('Incorrect number of parameters')
    number1.validate()
    number2.validate()
  }
}
