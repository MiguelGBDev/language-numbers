import { Calculator } from '../entities/calculator/calculator'

describe('When input is empty', () => {
  it('should return an error empty string', async () => {
    expect(() => Calculator.start('')).toThrowError('empty input')
  })
})

describe('when number is out of range', () => {
  it.each([
    // eslint-disable-next-line prettier/prettier
    '1000',
    '-1',
    'one thousand',
    'minus one',
    'mil',
    'menos uno'
  ])(`should return an error message for input %s`, invalidInput => {
    expect(() => Calculator.start(invalidInput)).toThrowError('some number is out of range (0-999)')
  })
})

describe('When input string is invalid', () => {
  it.each([
    // eslint-disable-next-line prettier/prettier
    'onehundred',
    'fortyfour',
    'cincuenta uno'
  ])('should return an error message for %s', invalidInput => {
    expect(() => Calculator.start(invalidInput)).toThrowError('invalid string')
  })
})

describe('when input is valid', () => {
  it.each([
    { input: '5', result: '5' },
    { input: '0 + 1', result: '1' },
    { input: 'five', result: 'five' },
    { input: 'zero plus one', result: 'one' },
    { input: 'nine hundred ninety-eight plus one', result: 'nine hundred ninety-nine' },
    { input: 'cinco', result: 'cinco' },
    { input: 'cero mas uno', result: 'uno' },
    { input: 'novecientos noventa y ocho mas uno', result: 'novecientos noventa y nueve' }
  ])('should return $result for $input', ({ input, result }) => {
    const output = Calculator.start(input)
    expect(output).toEqual(result)
  })
})
